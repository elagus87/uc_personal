/**
 * @file functions.cpp
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Funciones generales del proyecto.
 * @version 1.0
 * @date 2020-05-13
 * 
 */
#include "functions.h"
#include <Arduino.h>

/**
 * @brief estado controlado por interrupciones. 
 */
button_t bt_pressed=none;

/**
 * @fn leds_state()
 * @brief Encendido o apagado de leds dependiendo del estado expuesto en los argumentos.
 * @see pinout.h
 * 
 * @param led3 ON / OFF.
 * @param led2 ON / OFF.
 * @param led1 ON / OFF.
 */
void leds_state(char led3,char led2,char led1){
  if(led1)
    { digitalWrite(ledPin1,HIGH);}
  else 
    { digitalWrite(ledPin1,LOW); }
  if(led2)
    { digitalWrite(ledPin2,HIGH);}
  else 
    { digitalWrite(ledPin2,LOW); }
  if(led3)
    { digitalWrite(ledPin3,HIGH);}
  else 
    { digitalWrite(ledPin3,LOW); }
}

/**
 * @fn button_action()
 * @brief Según el botón presionado se hace el llamado a la función con los parametros asociados.
 * 
 */
void button_action(void)
{  
  // state manage by interruptions
  switch (bt_pressed) {
      case none:
        break;
      case boton1:
        bt_pressed = interface_action(ledPin1,buttonPin1);
        break;
      case boton2:
        bt_pressed = interface_action(ledPin2,buttonPin2);
        break;
      case boton3:
        bt_pressed = interface_action(ledPin3,buttonPin3);
        break;
  }
}

/**
 * @fn analog_read_send()
 * @brief Realiza una serie de mediciones para realizar el promedio de ellos. Si el valor hallado ha variado significativamente (1%), se envia el mensaje MIDI.
 * @see pinout.h
 * 
 * @param analog ,pin de lectura analógica.
 */
void analog_read_send(int analog)
{// read analog input and send midi message
  int analog_in=0;
  int averages=30;
  int idx=0;
  static int previous[6]={0};
  for (int i = 0; i < averages; i++)
  {
    analog_in+=analogRead(analog);
  }
  analog_in=analog_in/averages;

  idx=analog-54;
  if(previous[idx]+5<=analog_in || previous[idx]-5>=analog_in)
  {
    previous[idx]=analog_in;
    controlChange(0,70+idx, byte(analog_in/8));  //70 is Sound Controller
    MidiUSB.flush();
  }
}

/**
 * @fn interface_action()
 * @brief Realiza las acciones:
 * - Cambio de estado de los leds.
 * - Previene pulsaciones prolongadas.
 * - Envia mensajes MIDI correspondientes.
 * - Lleva FSM de button_action a su estado de espera.
 * 
 * @param led 
 * @param button 
 * @return button_t ,estado del button_action.
 */
button_t interface_action(int led,uint8_t button)
{
  static int action_state=OFF;
  static pressed_t button_st=IDLE;

  if(button_st==IDLE)
  { 
    if (action_state==OFF)
    {
      action_state=ON;
      digitalWrite(led,HIGH); 
      //16 to 19 is general purpose command
      controlChange(0, 16+((button-BASE)/DIF), 0);      
    }
    else
    {
      action_state=OFF;
      digitalWrite(led,LOW);
      controlChange(0, 16+((button-BASE)/DIF), 127);  
    }
    MidiUSB.flush();
    button_st=HOLD;      
  }
  else if (button_st==HOLD)
  { // if you stop pressing the button
    if(digitalRead(button))
    {button_st=FREE;}
  }
  else//FREE
  {
    button_st=IDLE;
    return (button_t)none;
  }
  return (button_t)boton1;
} 
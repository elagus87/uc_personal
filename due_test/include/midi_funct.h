#ifndef MIDI_FUNCT
#define MIDI_FUNCT

#include <MIDIUSB.h>
#include <Arduino.h>

void controlChange(byte channel, byte control, byte value);
void noteOn(byte channel, byte pitch, byte velocity);
void noteOff(byte channel, byte pitch, byte velocity);

#endif // MIDI_FUNCT

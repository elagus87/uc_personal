/**
 * @file pinout.h
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Pines para placa Arduino Due.
 * @version 1.0
 * @date 2020-05-13
 * 
 */
#ifndef PINOUTT
#define PINOUTT

#define buttonPin1 43  // the number of the Button pin
#define buttonPin2 47
#define buttonPin3 51 
// the number of the pushbutton pin
#define ledPin1 45    // the number of the LED pin
#define ledPin2 49
#define ledPin3 53     
// the number of the analog pin
#define pote1 A2
#define pote2 A5 //no anda
#define pote3 A1
#define slide1 A3
#define slide2 A4
#define slide3 A0

//general purpose defines
#define BASE 43 //first buttom
#define DIF 4   //distance between buttons

#endif // PINOUTT
# Arduino Due #

[![due](https://store-cdn.arduino.cc/usa/catalog/product/cache/1/image/520x330/604a3538c15e081937dbfbd20aa60aad/a/0/a000062_featured_1.jpg)](https://store.arduino.cc/usa/due)



- [Esquematicos](https://www.arduino.cc/en/uploads/Main/arduino-Due-schematic.pdf)

- [Pines](https://www.make.net.za/wp/wp-content/uploads/2016/08/Due-pinout-WEB.png)
/**
 * @file main.cpp
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Funciones centrales
 * @version 1.0
 * @date 2020-05-13
 */

/**
 * @mainpage Consola MIDI basada en Arduino Due
 * Consiste en una mino controladora MIDI 1.0 que cuenta con:
 * - 3 pulsadores con leds indicadores.
 * - 3 potenciometros.
 * - 3 potenciometros deslizables.
 * Arte de tapa de Magali Siviero (magalisiviero@gmail.com).
 * 
 */
#include <Arduino.h>
#include "functions.h"
#include "midi_funct.h"
#include "pinout.h"
#include "isr_func.h"

/**
 * @var input_AN
 * @brief Conjunto de entradas analógicas.
 * 
 */
int input_AN[6]={pote1,pote2,pote3,slide1,slide2,slide3};
/**
 * @fn void setup() 
 * @brief Configuraciones de perifericos.
 * @note respecto al USB:
 * .platformio\packages\framework-arduino-sam\cores\arduino\USB
 * esta ruta contiene USBcore.cpp que contiene los defines:
 * - USB_PRODUCT "Arduino Due"
 * - USB_MANUFACTURER "Arduino LLC"
 * 
 * Ambos con directivas de precompilación, por lo cual, definirlo aqui
 * da lugar a este valor. 
 * En caso de usar Platformio, ell el archivo .ini puede imponerse el valor
 * mediante:
 * - board_build.usb_product=Nombre a elección
 * 
 * dicha directiva anula este define 
 */
void setup() {  
  Serial.begin(115200);  // default midi speed rate
  // initialize the I/O ports:
  pinMode(ledPin1, OUTPUT);   
  pinMode(ledPin2, OUTPUT);  
  pinMode(ledPin3, OUTPUT); 
  pinMode(buttonPin1, INPUT); 
  pinMode(buttonPin2, INPUT);   
  pinMode(buttonPin3, INPUT);    
  // initialize led state 
  digitalWrite(ledPin1, LOW);         
  digitalWrite(ledPin2, LOW);    
  digitalWrite(ledPin3, LOW);    
  // interrupps
  attachInterrupt(digitalPinToInterrupt(buttonPin1),IRQ_debounce1, FALLING);
  attachInterrupt(digitalPinToInterrupt(buttonPin2),IRQ_debounce2, FALLING);
  attachInterrupt(digitalPinToInterrupt(buttonPin3),IRQ_debounce3, FALLING);  
  /* init sequence  */
  leds_state(1,1,1);  delay(1000); 
  leds_state(1,0,0);  delay(300);
  leds_state(0,1,0);  delay(300); 
  leds_state(0,0,1);  delay(300);
  leds_state(1,0,0);  delay(300);
  leds_state(0,1,0);  delay(300); 
  leds_state(0,0,1);  delay(300);
  leds_state(1,1,1);  delay(1000); 
  leds_state(0,0,0);
}
 
 /**
  * @fn loop()
  * @brief Equivalente a while(1) según se aprovechan los recursos Arduino.
  * 
  */
void loop()
{
/* buttons handled by interrupt */       
  button_action();
//--------------------------------------------------------//
   for(int i=0; i<6; i++)
  {
     analog_read_send(input_AN[i]);    
  }
 //--------------------------------------------------------//
}


  

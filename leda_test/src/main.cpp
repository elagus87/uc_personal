#include <Arduino.h>

#define BOTON1 PB12
#define BOTON2 PE12
#define LED PD8

void setup() {
  // put your setup code here, to run once:
  pinMode(BOTON1,INPUT);
  pinMode(BOTON2,INPUT);
  pinMode(LED,OUTPUT);
  digitalWrite(LED,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (!digitalRead(BOTON1))
  {digitalWrite(LED,HIGH);}
  if (!digitalRead(BOTON2))
  {digitalWrite(LED,LOW);}
  
}
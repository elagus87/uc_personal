#include <MIDIUSB.h>
#include <Arduino.h>
#include "midi_funct.h"


// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

// functions
void controlChange(byte channel, byte control, byte value) {
  byte event_id=0xB0;
  byte ch=event_id | channel;
  midiEventPacket_t event = {0x0B, ch, control, value};
  MidiUSB.sendMIDI(event);}

void noteOn(byte channel, byte pitch, byte velocity) {
  byte event_id=0x90;
  byte ch=event_id | channel;
  midiEventPacket_t noteOn = {0x09, ch, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  byte event_id=0x80;
  byte ch=event_id | channel;
  midiEventPacket_t noteOff = {0x09, ch, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

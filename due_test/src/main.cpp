/**
 * @file main.cpp
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Test de configuraciones USB básicas.
 * @version 0.1
 * @date 2020-04-29
 * 
 * @copyright Copyright (c) 2020
 * 
 */

/**
 * @mainpage Test de configuraciones USB para Arduino Due con PlatformIO.
 * 
 * @warning Despues de cambios en USB, debe desinstalarse el dispositivo para ver luego los cambios.
 * @image html due.jpg
 * @image html platform.png
 * 
 */
#include <Arduino.h>
#include "midi_funct.h"
#include <USB/USBCore.h>

/**
 * - @def USB_PRODUCT "Arduino Due"
 * - @def USB_MANUFACTURER "Arduino LLC"
 */
#define USB_PRODUCT "MIDI Agus"
#define USB_MIDI 0

/**
 * @fn void setup() 
 * @brief
 * .platformio\packages\framework-arduino-sam\cores\arduino\USB
 * esta ruta contiene USBcore.cpp que contiene los defines:
 * - USB_PRODUCT "Arduino Due"
 * - USB_MANUFACTURER "Arduino LLC"
 * 
 * Ambos con directivas de precompilación, por lo cual, definirlo aqui
 * da lugar a este valor. 
 * En caso de usar Platformio, ell el archivo .ini puede imponerse el valor
 * mediante:
 * - board_build.usb_product=Nombre a elección
 * 
 * dicha directiva anula este define 
 */
void setup() {
  // put your setup code here, to run once:
  #ifdef USB_MIDI
    Serial.begin(115200);
  #else
    Serial.begin(9600);
  #endif  
}
/**
 * @fn void loop()
 * @brief Loop principal, solo muestra el nombre del dispositivo
 * o envia mensajes MIDI según directivas de precompilación.
 * @details Directivas de precompilación
 * @note #ifdef, #else y #endif cmnpilará según la definición
 * o no de USB_MIDI. En este caso se puede testear la correcta 
 * definición de USB_PRODUCT o el funcionamiento de la comunicación MIDI.
 */
void loop() {
  // put your main code here, to run repeatedly:
  #ifdef USB_MIDI
    noteOn(0,69,127);
    delay(1000);
    noteOff(0,69,0);
    delay(1000);
  #else
    Serial.println(USB_PRODUCT);
    delay(1000);
  #endif
}
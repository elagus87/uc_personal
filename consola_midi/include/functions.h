/**
 * @file functions.h
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Definiciones validas para las funciones generales.
 * @version 1.0
 * @date 2020-05-13
 * 
 */
#ifndef FUNCTIONS
#define FUNCTIONS
#include <Arduino.h>
#include "pinout.h"
#include "midi_funct.h"

#define ON 1
#define OFF 0

/**
 * @enum button_t
 * Identificadores de pulsadores.
 */
enum button_t{none,
              boton1,
              boton2,
              boton3};
/**
 * @enum pressed_t
 * Posibles eventos de pulsación.
 */
enum pressed_t{IDLE,
              HOLD,
              FREE};

void leds_state(char led3,char led2,char led1);
void button_action(void);
void analog_read_send(int analog);
/* void interface_action(int led,uint8_t push, uint8_t button); */
button_t interface_action(int led,uint8_t button);


// button state
extern button_t bt_pressed;

#endif // FUNCTIONS

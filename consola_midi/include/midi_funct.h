/**
 * @file midi_funct.h
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Definiciones validas para las funciones MIDI.
 * @version 1.0
 * @date 2020-05-13
 * 
 */
#ifndef MIDI_FUNCT
#define MIDI_FUNCT

#include <MIDIUSB.h>
#include <Arduino.h>

void controlChange(byte channel, byte control, byte value);
void noteOn(byte channel, byte pitch, byte velocity);
void noteOff(byte channel, byte pitch, byte velocity);

#endif // MIDI_FUNCT

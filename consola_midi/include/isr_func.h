/**
 * @file isr_func.h
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Definiciones validas para las funciones IRQ.
 * @version 1.0
 * @date 2020-05-13
 * 
 */
#ifndef ISR_FUNC
#define ISR_FUNC
#include <Arduino.h>
#include "pinout.h"
#include "functions.h"

#define timeThreshold 150
void IRQ_debounce1(void);
void IRQ_debounce2(void);
void IRQ_debounce3(void);

extern int startTime1;
extern int startTime2;
extern int startTime3;

#endif // ISR_FUNC
/**
 * @file midi_funct.cpp
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Mensajes MIDI.
 * @version 1.0
 * @date 2020-05-13
 * 
 */
#include <MIDIUSB.h>
#include <Arduino.h>
#include "midi_funct.h"

/**
 * @fn controlChange()
 * @brief Comandos de control que no incluyen notas.
 * 
 * @param channel ,canal MIDI (0 a 15).
 * @param control ,acción	de control.
 * @param value ,especifico si amerita (ej. Modulation value).
 */
void controlChange(byte channel, byte control, byte value) {
  byte key=0xB0;
  byte ch=key | channel;
  midiEventPacket_t event = {0x0B, ch, control, value};
  MidiUSB.sendMIDI(event);}

/**
 * @fn noteOn()
 * @brief Comienzo de notas.
 * 
 * @param channel ,canal MIDI (0 a 15).
 * @param pitch ,número	de nota (ej. 69=La4=A4).
 * @param velocity ,intensidad	de la nota.
 */
void noteOn(byte channel, byte pitch, byte velocity) {
  byte key=0x90;
  byte ch=key | channel;
  midiEventPacket_t noteOn = {0x09, ch, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

/**
 * @fn noteOff()
 * @brief Comienzo de notas.
 * 
 * @param channel ,canal MIDI (0 a 15).
 * @param pitch ,número	de nota (ej. 69=La4=A4).
 * @param velocity ,pierde sentido pero el valor podria ser 0.
 */
void noteOff(byte channel, byte pitch, byte velocity) {
  byte key=0x80;
  byte ch=key | channel;
  midiEventPacket_t noteOff = {0x09, ch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

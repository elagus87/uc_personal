/**
 * @file isr_func.cpp
 * @author Agustin M. Ortiz (aortiz@est.frba.utn.edu.ar)
 * @brief Atención de interrupciones.
 * @version 1.0
 * @date 2020-05-13
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include "isr_func.h"
#include <Arduino.h>

/**
 * @brief Tiempos transcurridos para definir anti rebote.
 * @var startTime1
 * @var startTime2
 * @var startTime3
 */
int startTime1=0;
int startTime2=0;
int startTime3=0;

/**
 * @fn IRQ_debounce1()
 * @brief Disparado por la pulsación del boton 1; establece el estado luego de un tiempo de rebote. 
 */
void IRQ_debounce1(void)
{ // if the button 1 is pressed, it enters this function
	if (millis() - startTime1 > timeThreshold)
	{
		if(!digitalRead(buttonPin1)){bt_pressed=boton1;}
		else{bt_pressed=none;}
		startTime1 = millis();
	}
}

/**
 * @fn IRQ_debounce2()
 * @brief Disparado por la pulsación del boton 2; establece el estado luego de un tiempo de rebote. 
 */
void IRQ_debounce2(void)
{
	if (millis() - startTime2 > timeThreshold)
	{
		if(!digitalRead(buttonPin2)){bt_pressed=boton2;}
		else{bt_pressed=none;}
		startTime2 = millis();
	}
}

/**
 * @fn IRQ_debounce3()
 * @brief Disparado por la pulsación del boton 3; establece el estado luego de un tiempo de rebote. 
 */
void IRQ_debounce3(void)
{
	if (millis() - startTime3 > timeThreshold)
	{
		if(!digitalRead(buttonPin3)){bt_pressed=boton3;}
		else{bt_pressed=none;}
		startTime3 = millis();
	}
}